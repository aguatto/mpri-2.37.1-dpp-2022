# Deterministic Parallel Programming

## About

Multicore processors are ubiquitous, yet programs seldom exploit parallelism,
perhaps because writing correct parallel code is hard.

The goal of this lecture series is to introduce students to the idea of
deterministic parallel programming, which strikes a reasonable balance between
programmer productivity and machine efficiency.

We will use the [Cilk](http://cilk.mit.edu) extension of C and C++ as a vehicle
for the concepts taught in the course.

(These lectures are part of the [**MPRI
2.37.1**](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-37-1)
module and are given by [A. Guatto](mailto:guatto@irif.fr).)

## Documents

- The lecture material, including example Cilk programs, is in the
  [lecture](lecture/) directory.

- Some [exercises](exercises/exercises.pdf) will be made available.

## See also

L. Maranget's [part](http://pauillac.inria.fr/~maranget/MPRI/index.html) of
**MPRI 2.37.1** about weak memory models.
