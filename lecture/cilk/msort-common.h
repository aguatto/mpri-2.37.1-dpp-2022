#ifndef MSORT_H
#define MSORT_H

#include <stddef.h>

void merge_seq(int *C,
               const int *A, size_t A_lo, size_t A_hi,
               const int *B, size_t B_lo, size_t B_hi);
void merge_par(int *C,
               const int *A, size_t A_lo, size_t A_hi,
               const int *B, size_t B_lo, size_t B_hi);
void mergesort_seq(int *D, int *A, size_t lo, size_t hi);

#endif  /* MSORT_H */
