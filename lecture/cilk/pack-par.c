#include "array.h"
#include "common.h"
#include "detres.h"
#include "cmdline.h"

int *do_pack(size_t *pn, int *T, parameter_t *p) {
  size_t n = *pn /= 2;
  int *A = int_array(n), *B = int_array(n);
  bool *K = bool_array(n);

  cilk_for (int i = 0; i < n; i++) {
    A[i] = T[2*i];
    K[i] = T[2 * i + 1] % 2;
  }

  if (p->verbose) {
    print_int_array(A, n);
    print_bool_array(K, n);
  }

  start_timing();
  pack_par(B, A, K, n);
  stop_timing_print(n);

  if (p->verbose)
    print_int_array(B, n);

  free(A);
  free(K);

  return B;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_pack,
             "Pack an array in parallel");
  return 0;
}
