#ifndef CMDLINE_H
#define CMDLINE_H

#include "common.h"

typedef struct {
  char *in_file;                /* input file; may be NULL */
  char *out_file;               /* output file; may be NULL */
  bool verbose;                 /* print extra messages */
  int random_seed;              /* random seed; -1 when no specified */
  bool use_nondet_prng;         /* use a nondeterministic PRNG */
  bool check;                   /* perform additional checks on the output */
} parameter_t;

typedef int *(*action_t)(size_t *, int *, parameter_t *);

void usage(const char *progname, const char *description);
bool parse_command_line(int argc, char **argv, parameter_t *p);
void run_action(int argc, char **argv, action_t act, const char *description);

#endif  /* CMDLINE_H */
