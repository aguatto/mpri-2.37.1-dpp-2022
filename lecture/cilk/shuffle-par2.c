#include "array.h"
#include "common.h"
#include "detres.h"
#include "cmdline.h"

/* Code from MergeShuffle: A Very Fast, Parallel Random Permutation Algorithm */
/* Available at https://arxiv.org/abs/1508.03167 */

#define MERGE_SHUFFLE_CUTOFF (1 << 10)

void merge(int *t, size_t m, size_t n) {
  int *u = t;
  int *v = t + m;
  int *w = t + n;

  // randomly take elements of the first and second array according to flips
  while (1) {
    if (random_bit()) {
      if (v == w) break;
      swap(*u, *v);
      v++;
    } else
      if (u == v) break;
    u++;
  }

  // now one array is exhausted, use Fisher-Yates to finish merging
  while (u < w) {
    size_t i = random_int_per_thread(u - t);
    swap(*(t + i), *u);
    u++;
  }
}

void merge_shuffle_par(int *t, size_t n, unsigned int cutoff) {
  // select q = 2^c such that n/q <= cutoff
  unsigned int c = 0;
  while ((n >> c) > cutoff) c++;
  const size_t q = 1 << c;
  size_t nn = n;

  // divide the input in q chunks, use Fisher-Yates to shuffle them
  cilk_for (size_t i = 0; i < q; i++) {
    size_t j = nn * i >> c;
    size_t k = nn * (i+1) >> c;
    fy_shuffle_seq_inline_rand(t + j, k - j);
  }

  for (size_t p = 1; p < q; p += p) {
    // merge together the chunks in pairs
    const size_t r = 2 * p;
    cilk_for (size_t i = 0; i < q; i += r) {
      size_t j = nn * i >> c;
      size_t k = nn * (i + p) >> c;
      size_t l = nn * (i + 2 * p) >> c;
      merge(t + j, k - j, l - j);
    }
  }
}

int *do_shuffle(size_t *pn, int *H, parameter_t *p) {
  size_t n = *pn;
  int *A = int_array(n);

  cilk_for (int i = 0; i < n; i++) {
    A[i] = i;
  }

  if (p->verbose)
    printf("Shuffling %zu numbers using MergeShuffle\n", n);

  start_timing();
  merge_shuffle_par(A, n, MERGE_SHUFFLE_CUTOFF);
  stop_timing_print(n);

  return A;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_shuffle,
             "Generate a random shuffle using the MergeShuffle algorithm");
  return 0;
}
