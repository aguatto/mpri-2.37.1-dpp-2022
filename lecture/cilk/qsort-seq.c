#include "array.h"
#include "cmdline.h"
#include "common.h"
#include "detres.h"
#include "qsort-common.h"

int *do_quicksort(size_t *pn, int *A, parameter_t *p) {
  size_t i, n = *pn;

  start_timing();
  quicksort_seq(A, 0, n);
  stop_timing_print(n);

  if (p->check && (i = is_sorted(A, n)) < n) {
    fprintf(stderr,
            "not sorted: A[%zu] = %d > %d = A[%zu]\n",
            i, A[i], A[i + 1], i + 1);
    exit(EXIT_FAILURE);
  }

  return A;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_quicksort,
             "Sort the array using sequential quicksort");
}
