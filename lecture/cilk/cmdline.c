#include "cmdline.h"

void usage(const char *progname, const char *description) {
  fprintf(stderr, "Usage: %s [OPTIONS]\n", progname);
  fprintf(stderr, "%s\n\n", description);
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-i FILE\t\tread input from FILE\n");
  fprintf(stderr, "\t-o FILE\t\twrite output to FILE\n");
  fprintf(stderr, "\t-v     \t\tbe verbose\n");
  fprintf(stderr, "\t-s SEED\t\tinitialize prng with SEED\n");
  fprintf(stderr, "\t-n     \t\tuse a non-deterministic prng\n");
  fprintf(stderr, "\t-c     \t\tcheck results for correctness\n");
}

bool parse_command_line(int argc, char **argv, parameter_t *p) {
  int opt;

  assert (argv);
  assert (p);

  memset(p, 0, sizeof *p);
  p->random_seed = -1;

  srand(time(NULL));

  while ((opt = getopt(argc, argv, "i:o:vs:nc")) != -1) {
    switch (opt) {
    case 'i':
      p->in_file = strdup(optarg);
      break;

    case 'o':
      p->out_file = strdup(optarg);
      break;

    case 'v':
      p->verbose = true;
      break;

    case 's':
      p->random_seed = atoi(optarg);
      srand(p->random_seed);
      break;

    case 'n':
      p->use_nondet_prng = true;
      break;

    case 'c':
      p->check = true;
      break;

    default:
      return false;
    }
  }

  return true;
}

void run_action(int argc, char **argv, action_t act, const char *description) {
  bool same_pointer;
  parameter_t p;

  if (!parse_command_line(argc, argv, &p)) {
    usage(argv[0], description);
    exit(EXIT_FAILURE);
  }

  if (p.in_file == NULL)
    die("no input file specified\n");

  int *T, *R;
  size_t n;

  if (check_file_extension(p.in_file, "csv"))
    n = read_array_from_csv_file(p.in_file, &T);
  else
    n = read_array_from_file(p.in_file, &T);

  if ((R = act(&n, T, &p)) == NULL)
    die("failure");

  if (p.out_file != NULL) {
    if (check_file_extension(p.out_file, "csv")) {
      write_array_to_csv_file(p.out_file, R, n);
    } else {
      write_array_to_file(p.out_file, R, n);
    }
  }

  same_pointer = (R == T);
  free(R);
  if (!same_pointer)
    free(T);
  free(p.in_file);
  free(p.out_file);
}
