#ifndef QSORT_COMMON_H
#define QSORT_COMMON_H

int partition(int *A, int lo, int hi, int pivot);
void quicksort_seq(int *A, int lo, int hi);

#endif  /* QSORT_COMMON_H */
