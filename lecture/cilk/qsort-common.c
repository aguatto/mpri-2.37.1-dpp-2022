#include "common.h"
#include "qsort-common.h"

int partition(int *A, int lo, int hi, int pivot) {
  int i = lo;
  for (int j = lo; j < hi; j++) {
    if (A[j] <= pivot) {
      swap(A[j], A[i]);
      i++;
    }
  }
  return i;
}

void quicksort_seq(int *A, int lo, int hi) {
  if (hi - lo <= 1)
    return;

  int pivot = A[hi - 1];
  int k = partition(A, lo, hi, pivot);
  quicksort_seq(A, lo, k - 1);
  quicksort_seq(A, k - 1, hi);
}
