#include "msort-common.h"

#include "array.h"
#include "common.h"

#define MERGE_CUTOFF 1024

void merge_seq(int *C,
               const int *A, size_t A_lo, size_t A_hi,
               const int *B, size_t B_lo, size_t B_hi) {
  assert (C);
  assert (A);
  assert (B);

  size_t i = A_lo, j = B_lo;
  while (i < A_hi || j < B_hi)
    if (i < A_hi && (j == B_hi || A[i] <= B[j]))
      *C++ = A[i++];
    else
      *C++ = B[j++];
}

void merge_par(int *C,
               const int *A, size_t A_lo, size_t A_hi,
               const int *B, size_t B_lo, size_t B_hi) {
  assert (C);
  assert (A);
  assert (B);

  if (range_size(A_lo, A_hi) < range_size(B_lo, B_hi)) {
    merge_par(C, B, B_lo, B_hi, A, A_lo, A_hi);
    return;
  }

  if (range_size(A_lo, A_hi) <= 1
      || range_size(A_lo, A_hi) + range_size(B_lo, B_hi) <= MERGE_CUTOFF) {
    merge_seq(C, A, A_lo, A_hi, B, B_lo, B_hi);
    return;
  }

  size_t A_mid = midpoint(A_lo, A_hi);
  size_t B_mid = search_sorted(B, B_lo, B_hi, A[A_mid]);
  cilk_spawn merge_par(C, A, A_lo, A_mid, B, B_lo, B_mid);
  merge_par(C + range_size(A_lo, A_mid) + range_size(B_lo, B_mid),
            A, A_mid, A_hi, B, B_mid, B_hi);
  cilk_sync;
}

void mergesort_seq(int *B, int *A,
                   size_t lo, size_t hi) {
  switch (range_size(lo, hi)) {
  case 0: break;
  case 1: B[lo] = A[lo]; break;
  default:
    {
      size_t mid = midpoint(lo, hi);
      mergesort_seq(A, B, lo, mid);
      mergesort_seq(A, B, mid, hi);
      merge_seq(B + lo, A, lo, mid, A, mid, hi);
      break;
    }
  }
}
