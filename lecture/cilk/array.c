#include "array.h"
#include "common.h"

#define PACK_BLOCK_SIZE 1024
#define PSUM_BLOCK_SIZE 1024

int sum_bool_seq(const bool *A, size_t n) {
  int res = 0;
  for (int i = 0; i < n; i++)
    if (A[i])
      res++;
  return res;
}

int prefix_sum_seq_inclusive(int *B, const int *A, size_t n) {
  assert (B);
  assert (A);

  B[0] = A[0];
  for (int i = 1; i < n; i++) {
    B[i] = B[i-1] + A[i];
  }

  return B[n - 1];
}

int prefix_sum_seq_inclusive_inplace(int *A, size_t n) {
  assert (A);

  for (int i = 1; i < n; i++) {
    A[i] = A[i-1] + A[i];
  }

  return n > 0 ? A[n-1] : 0;
}

int prefix_sum_seq_exclusive(int *B, const int *A, size_t n) {
  assert (B);
  assert (A);

  int r = 0;

  for (int i = 0; i < n; i++) {
    B[i] = r;
    r += A[i];
  }

  return r;
}

int prefix_sum_seq_exclusive_inplace(int *A, size_t n) {
  assert (A);

  int r = 0, t;

  for (int i = 0; i < n; i++) {
    t = A[i];
    A[i] = r;
    r += t;
  }

  return r;
}

int prefix_sum_par_inclusive(int *B, const int *A, size_t n) {
  int b = nblocks(n, PSUM_BLOCK_SIZE);

  if (b <= 1)
    return prefix_sum_seq_inclusive(B, A, n);

  int *sums = malloc_checked(b * sizeof *sums);

  cilk_for (int i = 0; i < b; i++) {
    sums[i] = prefix_sum_seq_inclusive(B + i * PSUM_BLOCK_SIZE,
                                       A + i * PSUM_BLOCK_SIZE,
                                       block_size(i, PSUM_BLOCK_SIZE, n));
  }

  int res = prefix_sum_par_inclusive(sums, sums, b);

  cilk_for (int i = 1; i < b; i++) {
    for (int j = 0; j < block_size(i, PSUM_BLOCK_SIZE, n); j++) {
      B[i * PSUM_BLOCK_SIZE + j] += sums[i - 1];
    }
  }

  free(sums);

  return res;
}

int prefix_sum_par_inclusive_inplace(int *A, size_t n) {
  int b = nblocks(n, PSUM_BLOCK_SIZE);

  if (b <= 1)
    return prefix_sum_seq_inclusive_inplace(A, n);

  int *sums = malloc_checked(b * sizeof *sums);

  cilk_for (int i = 0; i < b; i++) {
    sums[i] =
      prefix_sum_seq_inclusive_inplace(A + i * PSUM_BLOCK_SIZE,
                                       block_size(i, PSUM_BLOCK_SIZE, n));
  }

  int res = prefix_sum_par_inclusive_inplace(sums, b);

  cilk_for (int i = 1; i < b; i++) {
    for (int j = 0; j < block_size(i, PSUM_BLOCK_SIZE, n); j++) {
      A[i * PSUM_BLOCK_SIZE + j] += sums[i - 1];
    }
  }

  free(sums);

  return res;
}

int prefix_sum_par_exclusive(int *B, const int *A, size_t n) {
  int b = nblocks(n, PSUM_BLOCK_SIZE);
  if (b <= 1)
    return prefix_sum_seq_exclusive(B, A, n);

  int *sums = malloc_checked(b * sizeof *sums);

  cilk_for (int i = 0; i < b; i++) {
    sums[i] = prefix_sum_seq_exclusive(B + i * PSUM_BLOCK_SIZE,
                                       A + i * PSUM_BLOCK_SIZE,
                                       block_size(i, PSUM_BLOCK_SIZE, n));
  }

  int res = prefix_sum_par_exclusive(sums, sums, b);

  cilk_for (int i = 1; i < b; i++) {
    for (int j = 0; j < block_size(i, PSUM_BLOCK_SIZE, n); j++) {
      B[i * PSUM_BLOCK_SIZE + j] += sums[i - 1];
    }
  }

  free(sums);

  return res;
}

int prefix_sum_par_exclusive_inplace(int *A, size_t n) {
  int b = nblocks(n, PSUM_BLOCK_SIZE);

  if (b <= 1)
    return prefix_sum_seq_exclusive_inplace(A, n);

  int *sums = malloc_checked(b * sizeof *sums);

  cilk_for (int i = 0; i < b; i++) {
    sums[i] =
      prefix_sum_seq_exclusive_inplace(A + i * PSUM_BLOCK_SIZE,
                                       block_size(i, PSUM_BLOCK_SIZE, n));
  }

  int res = prefix_sum_par_exclusive_inplace(sums, b);

  cilk_for (int i = 1; i < b; i++) {
    for (int j = 0; j < block_size(i, PSUM_BLOCK_SIZE, n); j++) {
      A[i * PSUM_BLOCK_SIZE + j] += sums[i - 1];
    }
  }

  free(sums);

  return res;
}

size_t pack_seq(int *dst, const int *src, const bool *keep, size_t n) {
  size_t res = 0;

  for (int i = 0; i < n; i++)
    if (keep[i])
      dst[res++] = src[i];

  return res;
}

size_t pack_par(int *dst, const int *src, const bool *keep, size_t n) {
  size_t b = nblocks(n, PACK_BLOCK_SIZE);

  if (b <= 1)
    return pack_seq(dst, src, keep, n);

  size_t res;
  int *sums = malloc_checked(b * sizeof *sums);

  cilk_for (size_t i = 0; i < b; i++) {
    sums[i] = sum_bool_seq(keep + i * PACK_BLOCK_SIZE,
                           block_size(i, PACK_BLOCK_SIZE, n));
  }

  res = prefix_sum_par_exclusive_inplace(sums, b);

  cilk_for (size_t i = 0; i < b; i++) {
    pack_seq(dst + sums[i],
             src + i * PACK_BLOCK_SIZE,
             keep + i * PACK_BLOCK_SIZE,
             block_size(i, PACK_BLOCK_SIZE, n));
  }

  free(sums);

  return res;
}

void fy_shuffle_seq(size_t n, int *A, const int *H) {
  for (size_t i = n - 1; i > 0; i--)
    swap(A[H[i]], A[i]);
}

void fy_shuffle_seq_inline_rand(int *A, size_t n) {
  for (int i = n - 1; i >= 0; i--) {
    int h = random_int(i);
    swap(A[i], A[h]);
  }
}

size_t is_sorted(const int *A, size_t n) {
  for (size_t i = 0; i < n - 1; i++)
    if (A[i] > A[i+1])
      return i;
  return n;
}

size_t search_sorted(const int *A, size_t lo, size_t hi, int x) {
  while (range_size(lo, hi) > 1) {
    size_t mid = midpoint(lo, hi);
    int y = A[mid];
    if (x <= y)
      hi = mid;
    else
      lo = mid;
  }

  if (range_size(lo, hi) == 1 && x <= A[lo])
    return lo;

  return hi;
}
