#include "cmdline.h"
#include "common.h"
#include "array.h"

int *do_scan(size_t *pn, int *A, parameter_t *p) {
  const size_t n = *pn;

  if (p->verbose)
    print_int_array(A, n);

  int *B = malloc(n * sizeof *B);
  if (!B)
    die("malloc failed");

  start_timing();
  prefix_sum_seq_inclusive(A, B, n);
  stop_timing_print(n);

  if (p->verbose)
    print_int_array(B, n);

  return B;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_scan, "Compute a prefix sum sequentially");
}
