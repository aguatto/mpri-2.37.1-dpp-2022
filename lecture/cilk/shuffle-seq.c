#include "array.h"
#include "common.h"
#include "cmdline.h"

int *do_shuffle(size_t *pn, int *H, parameter_t *p) {
  size_t n = *pn;
  int *A = malloc(n * sizeof *A);
  if (!A)
    die("malloc failed");

  for (int i = 0; i < n; i++) {
    A[i] = i;
  }

  if (p->verbose)
    printf("Shuffling %zu numbers using sequential Fisher-Yates\n", n);

  start_timing();
  fy_shuffle_seq(n, A, H);
  stop_timing_print(n);

  return A;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_shuffle,
             "Generate a random shuffle using the sequential "
             "Fisher-Yates algorithm");
  return 0;
}
