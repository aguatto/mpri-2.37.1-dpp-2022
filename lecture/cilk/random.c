#include "common.h"
#include "cmdline.h"

int *do_random(size_t *pn, int *A, parameter_t *p) {
  size_t n = *pn;

  if (n <= 0)
    die("empty array");

  *pn = n = A[0];

  if (p->verbose)
    printf("Generating %zu pseudorandom numbers (%s PRNG)\n", n,
           (p->use_nondet_prng ? "nondeterministic" : "deterministic"));

  int *R = malloc(n * sizeof *R);
  if (!R)
    die("malloc failed");

  start_timing();
  if (p->use_nondet_prng) {
    cilk_for (int i = 0; i < n; i++)
      R[i] = random_int_per_thread(i);
  } else {
    for (int i = 0; i < n; i++) {
      R[i] = random_int(i);
    }
  }
  stop_timing_print(n);

  return R;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_random,
             "Generate random numbers for the Fisher-Yates algorithm");
  return 0;
}
