#include "common.h"

unsigned int fib(unsigned int n) {
  unsigned int x, y;
  if (n <= 1) return 1;
  cilk_spawn x = fib(n - 1);
  y = fib(n - 2);
  cilk_sync;
  return x + y;
}

int main(int argc, char **argv) {
  unsigned int n = 23;

  if (argc > 1)
    n = atoi(argv[1]);

  start_timing();
  printf("fib(%u) = %u\n", n, fib(n));
  printf("%f\n", stop_timing());
}
