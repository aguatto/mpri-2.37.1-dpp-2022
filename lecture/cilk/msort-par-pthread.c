#include <pthread.h>
#include <limits.h>

#include "array.h"
#include "cmdline.h"
#include "common.h"
#include "detres.h"
#include "qsort-common.h"
#include "msort-common.h"

#define MSORT_CUTOFF 4096

struct merge_args {
  int *B;
  int *A;
  size_t lo;
  size_t hi;
};

void mergesort_par(int *B, int *A, size_t lo, size_t hi);

void *mergesort_par_stub(void *p) {
  struct merge_args *args = (struct merge_args *)p;
  mergesort_par(args->B, args->A, args->lo, args->hi);
  return NULL;
}

void mergesort_par(int *B, int *A, size_t lo, size_t hi) {
  pthread_t t;
  struct merge_args *pa = malloc(sizeof *pa);

  assert (pa);
  assert (B);
  assert (A);

  if (range_size(lo, hi) <= MSORT_CUTOFF) {
    quicksort_seq(B, lo, hi);
    return;
  }

  size_t mid = midpoint(lo, hi);
  // Do one recursive call in its own thread.
  pa->A = A;
  pa->B = B;
  pa->lo = lo;
  pa->hi = mid;
  if (pthread_create(&t, NULL,
                     mergesort_par_stub, pa))
    die("pthread_create()");
  // Do the second call sequentially.
  mergesort_par(A, B, mid, hi);
  // Wait for the spawned thread to terminate.
  if (pthread_join(t, NULL))
    die("pthread_join()");
  merge_seq(B + lo, A, lo, mid, A, mid, hi);
  free(pa);
}

int *do_mergesort(size_t *pn, int *A, parameter_t *p) {
  size_t i, n = *pn;
  int *B = int_array(n);
  copy_int_array(B, A, n);

  if (p->verbose)
    print_int_array(A, n);

  start_timing();
  mergesort_par(B, A, 0, n);
  stop_timing_print(n);

  if (p->verbose)
    print_int_array(B, n);

  if (p->check && (i = is_sorted(B, n)) < n) {
    fprintf(stderr,
            "not sorted: B[%zu] = %d > %d = B[%zu]\n",
            i, B[i], B[i + 1], i + 1);
    exit(EXIT_FAILURE);
  }

  return B;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_mergesort,
             "Sort the array using parallel mergesort");
}
