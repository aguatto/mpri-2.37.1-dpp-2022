#include "array.h"
#include "cmdline.h"
#include "common.h"
#include "detres.h"
#include "msort-common.h"

int *do_mergesort(size_t *pn, int *A, parameter_t *p) {
  size_t i, n = *pn;
  int *B = int_array(n);
  copy_int_array(B, A, n);

  if (p->verbose)
    print_int_array(A, n);

  start_timing();
  mergesort_seq(B, A, 0, n);
  stop_timing_print(n);

  if (p->verbose)
    print_int_array(B, n);

  if (p->check && (i = is_sorted(B, n)) < n) {
    fprintf(stderr,
            "not sorted: B[%zu] = %d > %d = B[%zu]\n",
            i, B[i], B[i + 1], i + 1);
    exit(EXIT_FAILURE);
  }

  return B;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_mergesort,
             "Sort the array using sequential mergesort");
}
