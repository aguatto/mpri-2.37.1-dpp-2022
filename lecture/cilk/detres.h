#ifndef DETRES_H
#define DETRES_H

#include <stdatomic.h>

#include "common.h"

typedef _Atomic int reserve_t;

static inline int res_read_sc(reserve_t *ptr) {
  return atomic_load(ptr);
}

static inline void res_write_sc(reserve_t *ptr, int val) {
  atomic_store(ptr, val);
}

static inline int res_read(reserve_t *ptr) {
  return atomic_load_explicit(ptr, memory_order_acquire);
}

static inline void res_write(reserve_t *ptr, int val) {
  atomic_store_explicit(ptr, val, memory_order_release);
}

static inline void res_init(reserve_t *ptr) {
  res_write(ptr, -1);
}

static inline int res_write_max(reserve_t *ptr, int val) {
  assert (val >= 0);

  int current = atomic_load_explicit(ptr, memory_order_acquire);
  while (current < val &&
         !atomic_compare_exchange_strong_explicit(ptr,
                                                  &current, val,
                                                  memory_order_acq_rel,
                                                  memory_order_relaxed)) {
    asm volatile ("pause");
  }

  assert (atomic_load_explicit(ptr, memory_order_relaxed) >= val);
  return max(current, val);
}

static inline int res_write_max_sc(reserve_t *ptr, int val) {
  assert (val >= 0);

  int current = atomic_load(ptr);
  while (current < val &&
         !atomic_compare_exchange_strong(ptr, &current, val)) {
    asm volatile ("pause");
  }

  assert (atomic_load(ptr) >= val);
  return max(current, val);
}

static inline int res_write_max_seq(reserve_t *ptr, int val) {
  assert (val >= 0);
  *ptr = max(*ptr, val);
  return *ptr;
}

#endif  /* DETRES_H */
