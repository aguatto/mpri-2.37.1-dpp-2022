#ifndef MSORT_H
#define MSORT_H

void merge_seq(int *dst,
               const int *A, size_t A_lo, size_t A_hi,
               const int *B, size_t B_lo, size_t B_hi);

#endif  /* MSORT_H */
