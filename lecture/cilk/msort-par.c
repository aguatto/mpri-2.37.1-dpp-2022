#include "array.h"
#include "cmdline.h"
#include "common.h"
#include "detres.h"
#include "qsort-common.h"
#include "msort-common.h"

#define MSORT_CUTOFF 4096

void mergesort_par(int *B, int *A, size_t lo, size_t hi) {
  assert (B);
  assert (A);

  if (range_size(lo, hi) <= MSORT_CUTOFF) {
    quicksort_seq(B, lo, hi);
    return;
  }

  size_t mid = midpoint(lo, hi);
  cilk_spawn mergesort_par(A, B, lo, mid);
  mergesort_par(A, B, mid, hi);
  cilk_sync;
  merge_par(B + lo, A, lo, mid, A, mid, hi);
}

int *do_mergesort(size_t *pn, int *A, parameter_t *p) {
  size_t i, n = *pn;
  int *B = int_array(n);
  copy_int_array(B, A, n);

  if (p->verbose)
    print_int_array(A, n);

  start_timing();
  mergesort_par(B, A, 0, n);
  stop_timing_print(n);

  if (p->verbose)
    print_int_array(B, n);

  if (p->check && (i = is_sorted(B, n)) < n) {
    fprintf(stderr,
            "not sorted: B[%zu] = %d > %d = B[%zu]\n",
            i, B[i], B[i + 1], i + 1);
    exit(EXIT_FAILURE);
  }

  return B;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_mergesort,
             "Sort the array using parallel mergesort");
}
