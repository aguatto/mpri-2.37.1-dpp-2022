#include "array.h"
#include "cmdline.h"
#include "common.h"
#include "detres.h"
#include "qsort-common.h"

const int GRAIN = 1<<14;

void quicksort_par(int *tab, int lo, int hi) {
  if (hi - lo <= GRAIN) {
    quicksort_seq(tab, lo, hi);
    return;
  }

  int pivot = tab[hi - 1];
  int k = partition(tab, lo, hi, pivot);
  cilk_spawn quicksort_par(tab, lo, k - 1);
  quicksort_par(tab, k - 1, hi);
  cilk_sync;
}

int *do_quicksort(size_t *pn, int *A, parameter_t *p) {
  size_t i, n = *pn;

  start_timing();
  quicksort_par(A, 0, n);
  stop_timing_print(n);

  if (p->check && (i = is_sorted(A, n)) < n) {
    fprintf(stderr,
            "not sorted: A[%zu] = %d > %d = A[%zu]\n",
            i, A[i], A[i + 1], i + 1);
    exit(EXIT_FAILURE);
  }

  return A;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_quicksort,
             "Sort the array using parallel quicksort");
}
