#include "common.h"
#include "cmdline.h"

void dk_shuffle_seq(int n, int *A, const int *H) {
  for (int i = n - 1; i >= 0; i--)
    swap(A[H[i]], A[i]);
}

int main(int argc, char **argv) {
  parameter_t p;

  if (!parse_command_line(argc, argv, &p)) {
    usage(argv[0],
          "Generate a random shuffle using the Durstenfeld-Knuth algorithm");
    return 1;
  }

  if (p.in_file == NULL)
    die("no input file specified\n");

  double elapsed;
  int *A, *H;
  int n = read_array_from_file(p.in_file, &H);
  A = malloc(n * sizeof *A);

  for (int i = 0; i < n; i++) {
    A[i] = i;
  }

  start_timing();
  dk_shuffle_seq(n, A, H);
  elapsed = stop_timing();

  printf("%f s (%f Mitems/s)\n", elapsed, (n / 1E6) / elapsed);

  return 0;
}
