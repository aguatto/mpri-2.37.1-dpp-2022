#include "common.h"

#define BUFFER_SIZE 255

void die(const char *message) {
  perror(message);
  exit(EXIT_FAILURE);
}

static struct timespec beg, end;

void start_timing() {
  if (clock_gettime(CLOCK_MONOTONIC, &beg) != 0)
    die("Could not get time.");
}

double stop_timing() {
  if (clock_gettime(CLOCK_MONOTONIC, &end) != 0)
    die("Could not get time.");

  double elapsed =
    end.tv_sec + (double)end.tv_nsec / 1E9
    - (beg.tv_sec + (double)beg.tv_nsec / 1E9);
  return elapsed;
}

double stop_timing_print(size_t n) {
  double elapsed = stop_timing();
  // Execution time, throughput
  printf("%f,%f\n", elapsed, (n / 1E6) / elapsed);
  return elapsed;
}

void print_int_array(const int *tab, size_t n) {
  printf("[");
  for (int i = 0; i < n; i++) {
    printf("%d", tab[i]);
    if (i < n - 1) {
      printf(", ");
    }
  }
  printf("]\n");
}

void print_bool_array(const bool *tab, size_t n) {
  printf("[");
  for (int i = 0; i < n; i++) {
    printf("%d", tab[i]);
    if (i < n - 1) {
      printf(", ");
    }
  }
  printf("]\n");
}

size_t read_array_from_csv_file(const char *filename, int **A) {
  // Quick and dirty two-pass CSV parsing code. Beware!
  char c;
  size_t n = 1;
  FILE *f = fopen(filename, "r");

  if (!f)
    die("could not open file");

  while (fread(&c, sizeof c, 1, f) == 1)
    if (c == ',')
      n++;
  rewind(f);

  *A = malloc(n * sizeof **A);
  int i = 0;
  if (!*A)
    die("malloc failed");

  (*A)[0] = 0;
  while (fread(&c, sizeof c, 1, f) == 1) {
    if (c == ',')
      (*A)[++i] = 0;
    else if ('0' <= c && c <= '9') {
      (*A)[i] *= 10;
      (*A)[i] += c - '0';
    }
  }

  fclose(f);

  return n;
}

size_t read_array_from_file(const char *filename, int **A) {
  size_t n, m;
  FILE *f = fopen(filename, "rb");

  if (f == NULL)
    die("Could not open file");

  if (fseek(f, 0, SEEK_END) != 0)
    die("Could not seek in file");

  if ((n = ftell(f)) == -1)
    die("Could not find position in file");

  n /= sizeof **A;
  *A = malloc(n * sizeof **A);

  rewind(f);

  if ((m = fread(*A, sizeof **A, n, f)) != n) {
    fprintf(stderr, "Read %zu/%zu items from file\n", m, n);
    die("Could not read from file");
  }

  fclose(f);

  return n;
}

void write_array_to_file(const char *filename, const int *tab, size_t n) {
  FILE *f = fopen(filename, "wb+");

  if (f == NULL) {
    fprintf(stderr, "Could not open file [%s]\n", filename);
    exit(EXIT_FAILURE);
  }

  if (fwrite(tab, sizeof *tab, n, f) != n) {
    fprintf(stderr, "Could not write %zu items to [%s]\n", n, filename);
    exit(EXIT_FAILURE);
  }

  fclose(f);
}

void write_array_to_csv_file(const char *filename, const int *tab, size_t n) {
  char buffer[BUFFER_SIZE];
  FILE *f = fopen(filename, "w+");

  if (f == NULL) {
    fprintf(stderr, "Could not open file [%s]\n", filename);
    exit(EXIT_FAILURE);
  }

  for (int i = 0; i < n; i++) {
    snprintf(buffer, BUFFER_SIZE, "%d", tab[i]);
    fwrite(buffer, sizeof *buffer, strlen(buffer), f);
    if (i < n - 1)
      fwrite(",", sizeof(char), 1, f);
  }
  fwrite("\n", sizeof(char), 1, f);

  fclose(f);
}

char *file_extension(const char *filename) {
  char *res = strrchr(filename, '.');
  if (res == NULL)
    return NULL;
  else
    return res + 1;
}

bool check_file_extension(const char *filename, const char *ext) {
  char *suffix = file_extension(filename);
  return suffix != NULL && strcmp(suffix, ext) == 0;
}
