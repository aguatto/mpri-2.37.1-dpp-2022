#ifndef ARRAY_H
#define ARRAY_H

#include <stdbool.h>
#include <stddef.h>

int sum_bool_seq(const bool *A, size_t n);

int prefix_sum_seq_inclusive(int *B, const int *A, size_t n);
int prefix_sum_seq_inclusive_inplace(int *A, size_t n);
int prefix_sum_seq_exclusive(int *B, const int *A, size_t n);
int prefix_sum_seq_exclusive_inplace(int *A, size_t n);

int prefix_sum_par_inclusive(int *B, const int *A, size_t n);
int prefix_sum_par_inclusive_inplace(int *A, size_t n);
int prefix_sum_par_exclusive(int *B, const int *A, size_t n);
int prefix_sum_par_exclusive_inplace(int *A, size_t n);

size_t pack_seq(int *dst, const int *src, const bool *keep, size_t n);

/* Given two arrays of integers dst and src, and an array of booleans keep, all
   of size n, pack_par(dst, src, keep, n) copies into dst all the elements
   src[i] such that keep[i] is true, in order. It returns the number of copied
   elements. */
size_t pack_par(int *dst, const int *src, const bool *keep, size_t n);

void fy_shuffle_seq_inline_rand(int *A, size_t n);
// H[i] should contain a number in the [0, i] range.
void fy_shuffle_seq(size_t n, int *A, const int *H);

size_t is_sorted(const int *A, size_t n);

/* Given a sorted slice [lo, hi) of array A, search_sorted(A, lo, hi, x) returns
   the least i in [lo, hi] such that for all j in [i, hi), A[j] >= x. */
size_t search_sorted(const int *A, size_t lo, size_t hi, int x);

#endif
