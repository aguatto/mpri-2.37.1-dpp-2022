#include "cmdline.h"
#include "common.h"
#include "array.h"

int *do_scan(size_t *pn, int *A, parameter_t *p) {
  const size_t n = *pn;
  bool inplace = true;

  if (p->verbose)
    print_int_array(A, n);

  int *B = malloc(n * sizeof *B);
  if (!B)
    die("malloc failed");

  start_timing();
  if (inplace)
    prefix_sum_par_inclusive_inplace(A, n);
  else
    prefix_sum_par_inclusive(B, A, n);
  stop_timing_print(n);

  if (p->verbose) {
    if (inplace)
      print_int_array(A, n);
    else
      print_int_array(B, n);
  }

  return A;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_scan, "Compute a prefix sum in parallel");
}
