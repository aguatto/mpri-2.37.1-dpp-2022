#!/usr/bin/env bash

set -e

while [ $# -gt 0 ]
do
    CONFIG="$1"
    shift

    if [ ! -f $CONFIG ]
    then
        echo "File $CONFIG does not exist" &1>2
        exit 1
    fi
    source $CONFIG

    if [ -z "$RUNS" ]
    then
        echo 'Config file should define $RUNS' &1>2
        exit 1
    fi

    if [ -z "$PERMS" ]
    then
        echo 'Config file should define $PERMS' &1>2
        exit 1
    fi

    if [ -z "$PROGS" ]
    then
        echo 'Config file should define $PROGS' &1>2
        exit 1
    fi

    if [ -z "$SIZES" ]
    then
        echo 'Config file should define $SIZES' &1>2
        exit 1
    fi

    TMP=`mktemp --suffix .csv`
    FILES=""
    RESDIR="`hostname`-results"
    RESULTS="`basename $CONFIG .bench`-`date \"+%Hh%Mm_%d-%m-%y\"`.csv"

    cd `dirname $(realpath $0)`

    if [ ! -d $RESDIR ]
    then
        mkdir $RESDIR
    else
        rm -f -- $RESDIR/*.csv
    fi

    for s in $SIZES
    do
        echo "$s" > "$TMP"
        FILE="/tmp/$s"
        for p in `seq 1 $PERMS`
        do
            echo "[Generating $s B ($p/$PERMS)]"
            [ $PERMS = 1 -a -f $FILE ] || ./random.bin -i $TMP -o $FILE >/dev/null

            echo "[Running benchmarks]"
            for p in $PROGS
            do
                OUT="$RESDIR/`basename $p .bin`.csv"
                for r in `seq 1 $RUNS`
                do
                    echo -n "$p,`basename $FILE .csv`," | tee -a $OUT
                    ./$p -i $FILE | tee -a $OUT
                done
            done
        done
    done
    echo '"Program","Size","Runtime","Throughput"' > $RESULTS
    cat $RESDIR/*.csv >> $RESULTS
    echo "[Output in $RESULTS]"
done
