#include "array.h"
#include "common.h"
#include "detres.h"
#include "cmdline.h"

// This code has been adapted from Guy Blelloch's implementation in the
// Problem-Based Benchmark Suite.

#define SHUFFLE_PAR_CUTOFF (1 << 14)

size_t fy_shuffle_par(size_t n, int *A, const int *H) {
  const size_t ratio = 100, max_round_size = 1 + n / ratio;
  size_t round_size = max_round_size;
  size_t hi = n, rounds = 0, wasted = 0;
  reserve_t *const R = malloc_checked(n * sizeof *R);
  int *const I = malloc_checked(n * sizeof *I);
  bool *const K = malloc_checked(max_round_size * sizeof *K);
  int *const V = malloc_checked(max_round_size * sizeof *V);

  // We maintain the invariant that R[i] == i at the beginning of every
  // round. This way, we can reserve with a single write_max().

  cilk_for (size_t i = 0; i < n; i++) {
    assert (0 <= H[i] && H[i] <= i);
    I[i] = i;
    res_init(&R[i]); // Invariant.
  }

  while (hi > SHUFFLE_PAR_CUTOFF) {
    rounds++;
    size_t actual_size = min(round_size, hi);
    size_t lo = hi - actual_size;

    // Reserve phase.
    cilk_for (size_t i = 0; i < actual_size; i++) {
      int idx = I[i + lo];
      int idy = H[idx];
      res_write_max(&R[idy], idx);
      res_write_max(&R[idx], idx);
    }

    // Commit phase.
    cilk_for (size_t i = 0; i < actual_size; i++) {
      int idx = I[i + lo];
      int idy = H[idx];
      K[i] = 1;
      V[i] = idx;
      if (res_read(&R[idy]) == idx) {
        if (res_read(&R[idx]) == idx) {
          swap(A[idx], A[idy]);
          K[i] = 0;
          res_init(&R[idx]);
        }
        res_init(&R[idy]);
      }
    }

    size_t kept = pack_par(I + lo, V, K, actual_size);
    hi -= actual_size - kept;
    wasted += kept;

    // Adjust round size based on the number of failed iterates.
    if ((float)kept / (float)actual_size > .2f)
      round_size = max(round_size / 2, max(round_size / 64 + 1, kept));
    else if ((float)kept / (float)actual_size < .1f)
      round_size = min(round_size * 2, max_round_size);
  }

  // Finish the remaining iterations sequentially.
  for (size_t i = hi - 1; i > 0; i--) {
    int idx = I[i];
    int idy = H[idx];
    swap(A[idx], A[idy]);
  }

  free(R);
  free(I);
  free(K);
  free(V);

  return rounds;
}

int *do_shuffle(size_t *pn, int *H, parameter_t *p) {
  size_t n = *pn;
  int *A = int_array(n);

  cilk_for (size_t i = 0; i < n; i++) {
    A[i] = i;
  }

  if (p->verbose)
    printf("Shuffling %zu numbers using DKSGBFG\n", n);

  start_timing();
  fy_shuffle_par(n, A, H);
  stop_timing_print(n);

  return A;
}

int main(int argc, char **argv) {
  run_action(argc, argv, do_shuffle,
             "Generate a random shuffle using the parallel "
             "Fisher-Yates algorithm");
  return 0;
}
