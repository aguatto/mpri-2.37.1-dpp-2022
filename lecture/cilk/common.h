#ifndef COMMON_H
#define COMMON_H

#include <assert.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#ifndef thread_local
#define thread_local _Thread_local
#endif  /* thread_local */

#ifndef NO_CILK
#include "cilk/cilk.h"
#else
#define cilk_spawn
#define cilk_sync
#define cilk_for for
#endif  /* NO_CILK */

#define swap(x, y) { typeof(x) _tmp = x; x = y; y = _tmp; }
#define max(x, y) (((x) <= (y)) ? (y) : (x))
#define min(x, y) (((x) <= (y)) ? (x) : (y))
#define midpoint(lo, hi) (((lo) + (hi)) / 2)
#define range_size(lo, hi) ((lo) >= (hi) ? 0 : (hi) - (lo))

#define nblocks(n, bsize) (1 + ((n) - 1) / (bsize))
#define block_size(i, bsize, n) (min((bsize), (n) - (i) * (bsize)))

void die(const char *);

void start_timing();
double stop_timing();
double stop_timing_print(size_t n);

void print_int_array(const int *, size_t);
void print_bool_array(const bool *, size_t);
size_t read_array_from_csv_file(const char *, int **);
size_t read_array_from_file(const char *, int **);
void write_array_to_file(const char *, const int *, size_t);
void write_array_to_csv_file(const char *, const int *, size_t);
char *file_extension(const char *);
bool check_file_extension(const char *filename, const char *ext);

static inline unsigned int hashU(unsigned int a)
{
   a = (a+0x7ed55d16) + (a<<12);
   a = (a^0xc761c23c) ^ (a>>19);
   a = (a+0x165667b1) + (a<<5);
   a = (a+0xd3a2646c) ^ (a<<9);
   a = (a+0xfd7046c5) + (a<<3);
   a = (a^0xb55a4f09) ^ (a>>16);
   return a;
}

static inline int hash(int a) {
  return hashU((unsigned int)a) & (((unsigned)1 << 31) - 1);
}

static inline int int_log2(int n) {
  int res;
  for (res = 0; n >>= 1; res++)
    ;
  return res;
}

static inline void *malloc_checked(size_t size) {
  void *p = malloc(size);
  if (!p)
    die("malloc failed\n");
  return p;
}

static inline int *int_array(size_t n) {
  return (int *)malloc_checked(n * sizeof(int));
}

static inline bool *bool_array(size_t n) {
  return (bool *)malloc_checked(n * sizeof(bool));
}

static inline int *copy_int_array(int *dst, const int *src, size_t n) {
  return (int *)memcpy(dst, src, n * sizeof *dst);
}

#define RAND_BUFFER_SIZE 256

static thread_local struct random_data state;
static thread_local char sbuf[RAND_BUFFER_SIZE];
static thread_local bool initialized = false;

static inline int random_int(int n) {
  return rand() % (n + 1);
}

static inline bool random_bit() {
  return (bool)random_int(1);
}

/* This function is useful for fast thread-safe pseudorandom number generation.

   Unfortunately, it interacts poorly with Cilk-style parallelism: the stream of
   pseudorandom numbers observed by the program depends on scheduling choices
   and is thus non deterministic. A better solution would be something in the
   vein of DotMix. */
static inline int random_int_per_thread(int n) {
  int res;

  if (!initialized) {
    if (initstate_r(rand(), sbuf, RAND_BUFFER_SIZE, &state) != 0)
      die("initstate_r() failed");
    initialized = true;
  }

  if (random_r(&state, &res) != 0)
    die("random_r() failed");

  return res % (n + 1);
}

static inline bool random_bit_per_thread() {
  return (bool)random_int(1);
}

#endif  /* COMMON_H */
