#!/usr/bin/env bash

set -e

F=/tmp/test.csv
P=/tmp/perm.csv

echo 51250 > $F
#echo 2000000 > $F
#echo 8192 > $F
while true
do
    ./random.bin -i $F -o $P
    ./shuffle-seq.bin -i $P -o out1.csv
    ./shuffle-par.bin -i $P -o out2.csv
    md5sum $P out1.csv out2.csv
    if [ `md5sum out1.csv | cut -f1 -d ' '` != `md5sum out2.csv | cut -f1 -d ' '` ]
    then
        exit 0
    fi
done
