# Lectures notes

This directory holds:

- the [lecture slides](slides.pdf),

- some [Cilk programs](cilk/).
